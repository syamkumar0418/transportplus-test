import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VehicleListComponent } from './vehicle-list/vehicle-list.component';
import { VehicleTrackingComponent } from './vehicle-tracking/vehicle-tracking.component';
import { LoginComponent } from './login/login.component';
const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'vehicle-tracking/:id',
    component: VehicleTrackingComponent
  },
  {
    path: 'vehicle-list',
    component: VehicleListComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
