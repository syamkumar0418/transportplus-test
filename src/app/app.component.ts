import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  isSidebarShow=false;
  constructor(private location: Location,private router: Router) { 
       
  }
  ngOnInit(): void {
    this.router.events.subscribe(() => {
      console.log(this.location.path());
      if(this.location.path()!="/" && this.location.path()!=""){
        this.isSidebarShow=true;
      }else{
        this.isSidebarShow=false;
      }   
    });
  }
}
