import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  data=[{name:"car",id:1,type:"LMV"},
              {name:"bus",id:2,type:"HMV"},
              {name:"bike",id:3,type:"LMV"},
              {name:"scooter",id:4,type:"LMV"},
              {name:"truck",id:5,type:"HMV"}]
  constructor(private http: HttpClient) { }
  getVehicleList() {
    //service for get vehicle list using http
    
    return this.data;
  }
  getVehicle(id){
    var data= this.data.filter((item, index) => item.id == id);
    return data;
  }
}
