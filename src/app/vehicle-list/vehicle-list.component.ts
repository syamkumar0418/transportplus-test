import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.scss']
})
export class VehicleListComponent implements OnInit {
  listVehicles: any=[];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    //use this when work with real service
    // this.dataService.getVehicleList().subscribe(
    //   data => this.listVehicles = data 
    // );

    this.listVehicles = this.dataService.getVehicleList();
  }

}
