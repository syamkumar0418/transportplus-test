import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from "@angular/router";
@Component({
  selector: 'app-vehicle-tracking',
  templateUrl: './vehicle-tracking.component.html',
  styleUrls: ['./vehicle-tracking.component.scss']
})
export class VehicleTrackingComponent implements OnInit {

  vehicle: Object;
  vehicleId:number;
  constructor(private route: ActivatedRoute, private data: DataService) { 
     this.route.params.subscribe( params => this.vehicleId = params.id );
  }

  ngOnInit() {
    
    this.vehicle=this.data.getVehicle(this.vehicleId)[0];
    console.log(this.vehicle);
  }


}
